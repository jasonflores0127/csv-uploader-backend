import {Entity, model, property} from '@loopback/repository';

@model()
export class Info extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
  })
  Year?: number;

  @property({
    type: 'number',
  })
  Rank?: number;

  @property({
    type: 'string',
  })
  Recipient?: string;

  @property({
    type: 'string',
  })
  Country?: string;

  @property({
    type: 'string',
  })
  Career?: string;

  @property({
    type: 'string',
  })
  Tied?: string;

  @property({
    type: 'string',
  })
  Title?: string;


  constructor(data?: Partial<Info>) {
    super(data);
  }
}

export interface InfoRelations {
  // describe navigational properties here
}

export type InfoWithRelations = Info & InfoRelations;
