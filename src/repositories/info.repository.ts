import {inject} from '@loopback/core';
import {DefaultCrudRepository} from '@loopback/repository';
import {InfodbDataSource} from '../datasources';
import {Info, InfoRelations} from '../models';

export class InfoRepository extends DefaultCrudRepository<
  Info,
  typeof Info.prototype.id,
  InfoRelations
> {
  constructor(
    @inject('datasources.infodb') dataSource: InfodbDataSource,
  ) {
    super(Info, dataSource);
  }
}
